import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import * as RootNavigation from '../services/RootNavigation';
import * as firebase from 'firebase'
import {connect} from 'react-redux'
import * as HealthData from '../services/HealthData'
import Footer from './Footer'
import Profile_IOS from './profile_ios'
import Profile_ANDROID from './profile_android'

declare var global: {HermesInternal: null | {}};

interface Props {
  displayName: String;
  email: String;
  isLoggedIn: boolean;
  operating_system: any;
}
class Profile extends React.Component<Props> {
  componentDidMount(){
  }

  signOutUser = () => {
    firebase.auth().signOut();
  };


  render() {
    // conditionally rendering depending on the OS
    let os_profile;
    if(this.props.operating_system == 'ios'){
      os_profile = <Profile_IOS></Profile_IOS>
    } else {
      os_profile = <Profile_ANDROID></Profile_ANDROID>
    }
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.wrapper}>
          <View style={styles.meta_wrapper}>
            <Image style={styles.profile_image} source={require('../assets/profile.jpg')} />
            <Text style={styles.profile_name}>{this.props.displayName}</Text>
          </View>

          <View style={styles.content_wrapper}>
            {os_profile}
          </View>
          {this.props.isLoggedIn ? <Footer></Footer> : null}
        </SafeAreaView>
      </>
    );
  }
};
function mapStateToProps(state:any) {
  return {
    email: state.email, 
    displayName: state.displayName,
    isLoggedIn: state.isLoggedIn,
    operating_system: state.operating_system
  }
}
export default connect(mapStateToProps)(Profile)




const styles = StyleSheet.create({
    wrapper: {
      flex: 0,
      flexDirection: 'column',
      color: 'black',
      backgroundColor: 'white',
      height: '100%'
    },
    meta_wrapper: {
      flexDirection: 'row',
      flex: 1
    },
    profile_image: {
      height: 120,
      borderRadius: 120,
      marginLeft: 30,
      marginTop: 30,
      flexBasis: 120
    },
    profile_name: {
      flex: 1,
      fontSize: 25,
      paddingTop: 50,
      marginLeft: 30,
      color: '#009688'
    },
    content_wrapper: {
      flex: 3,
      padding: 30,
      marginTop: 50
    },
    menu_wrapper: {
      flex: 1,
      justifyContent: 'flex-end',

    },
    menu_button: {
      width: '100%',
      borderTopColor: 'grey',
      borderTopWidth: 1,
      fontSize: 15,
      padding: 15
    },
    Footer: {
      flex:5
    }
});