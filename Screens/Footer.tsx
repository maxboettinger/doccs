import 'react-native-gesture-handler';
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import * as RootNavigation from '../services/RootNavigation';


declare var global: {HermesInternal: null | {}};

const Footer = ({navigation}:any) => {
  return (
    <>
      <View style={styles.footer}>
          <TouchableOpacity style={styles.recents} onPress={() => RootNavigation.navigate('Feed')}>
            <Text style={styles.footerButtonText}>Feed</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.nearby} onPress={() => RootNavigation.navigate('Profile')}>
            <Text style={[styles.footerButtonText, {paddingLeft: 100}]}>Profile</Text>
          </TouchableOpacity>

          <View style={styles.addButton}>
            <Text style={styles.addText}>+</Text>
          </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  footer: {
    flex: 0.5,
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    borderTopWidth: 1,
    borderTopColor: 'grey',
    
  },
  recents: {
    //flex: 2,
    width: '50%', 
  },
  nearby: {
    //flex: 2,
    width: '50%', 
  },
  footerButtonText: {
    fontSize: 18,
    paddingTop: 20,
    paddingLeft: 50
  },
  addButton: {
    backgroundColor: '#009688',
    borderRadius: 500,
    position: 'absolute',
    left: '50%',
    marginLeft: -50,
    width: 90,
    height: 90,
    marginTop: -20
  },
  addText: {
    fontSize: 60,
    margin: 'auto',
    textAlign: 'center', 

    color: 'white'
  }
});

export default Footer;
