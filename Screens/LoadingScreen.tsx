import React from "react";
import { View, Text, ActivityIndicator, StyleSheet } from "react-native";
import * as firebase from "firebase";
import {connect} from 'react-redux'
import * as RootNavigation from '../services/RootNavigation';
import { Platform} from 'react-native'

interface Props {
    displayName: string;
    email: string;
    isLoggedIn: boolean;
    operating_system: any;
    setUserInfo: (value:object) => void;
}
class LoadingScreen extends React.Component<Props> {
    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            if(user) {
                this.props.setUserInfo({
                    email: user.email,
                    isLoggedIn: true,
                    displayName: user.displayName,
                    operating_system: Platform.OS
                })

                RootNavigation.navigate("Feed")
            } else {
                RootNavigation.navigate("Login")
            }
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>Loading</Text>
                <ActivityIndicator size="large"></ActivityIndicator>
            </View>
        );
    }
}
function mapStateToProps(state:any) {
    return {
      email: state.email, 
      displayName: state.displayName,
      isLoggedIn: state.isLoggedIn,
      operating_system: state.operating_system
    }
}
function mapDispatchToProps(dispatch:any) {
    return {
        setUserInfo: (value:object) => dispatch({ type: 'SET_USER_INFO', value: value }),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoadingScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    }
});
