import 'react-native-gesture-handler';
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import {connect} from 'react-redux'
//import * as HealthData from '../services/HealthData'

import Fitness from '@ovalmoney/react-native-fitness';
const permissions = [
  { kind: Fitness.PermissionKind.Step, access: Fitness.PermissionAccess.Read },
];
Fitness.isAuthorized(permissions)
  .then((authorized:any) => {
    console.log('authorized', authorized)
    Fitness.getSteps({ startDate: '2020-01-01', endDate: '2020-06-01' }).then((data:any) => {
      console.log('data', data)
    })
  })
  .catch((error:any) => {
    // Do something
    console.log('error ', error)
  });

declare var global: {HermesInternal: null | {}};

interface Props {
  displayName: String;
  email: String;
  isLoggedIn: boolean;
}

class Profile_IOS extends React.Component<Props> {
  
  DOB:any

  componentDidMount(){
    this.getHealthData()
  }

  async getHealthData() {
    

    console.log("- - - ALL OF HEALTH DATA - - - ")
    console.log("DOB: ", this.DOB)
  }

  render() {
    return (
      <>
        <View style={styles.os_wrapper}>
            <Text>This is displayed on iOS</Text>
        </View>
      </>
    );
  }
};
function mapStateToProps(state:any) {
  return {
    email: state.email, 
    displayName: state.displayName,
    isLoggedIn: state.isLoggedIn,
  }
}
export default connect(mapStateToProps)(Profile_IOS)




const styles = StyleSheet.create({
    os_wrapper: {
        backgroundColor: 'red',
        flex: 1
    }
});