import 'react-native-gesture-handler';
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import {connect} from 'react-redux'

// This is my iOS Health Data Service. Maybe create one for android aswell. Probably replaced by our "wrapper" at some point
//import * as HealthData from '../services/HealthData'


declare var global: {HermesInternal: null | {}};

// Interface for the local state. All global fields that want to be used in this component need to be there
interface Props {
  displayName: String;
  email: String;
  isLoggedIn: boolean;
}
class Profile_ANDROID extends React.Component<Props> {
  componentDidMount(){

  }

  render() {
    return (
      <>
        <View style={styles.os_wrapper}>
            <Text>This is displayed on Android</Text>
        </View>
      </>
    );
  }
};
// function to map global REDUX state to the local component state
function mapStateToProps(state:any) {
  return {
    email: state.email, 
    displayName: state.displayName,
    isLoggedIn: state.isLoggedIn,
  }
}
// linking the global state with the local component state with usage of the above function
export default connect(mapStateToProps)(Profile_ANDROID)




const styles = StyleSheet.create({
    os_wrapper: {
        backgroundColor: 'red',
        flex: 1, // should be there to use the maximum space there is, depending on the parent component (profile page)
    }
});