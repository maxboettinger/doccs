import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity
} from 'react-native';
import * as firebase from 'firebase';
import * as RootNavigation from '../services/RootNavigation';

export default class LoginScreen extends React.Component {
  state = {
    email: '',
    password: '',
    errorMessage: null,
    isLoggedIn: false
  };

  handleLogin = () => {
    const {email, password} = this.state;

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(()=>{
        this.setState({
          isLoggedIn: true
        })
      })
      .catch((error:any) => this.setState({errorMessage: error.message}));
  };


// 72. RootNavigation.navigate('Register')}> /* go to register screen from login*
  render() {
    
    return (
      <View style={styles.container}>
        <Text style={styles.greeting}>
          {'Hello again. \n Welcome back. '}
        </Text>

        <View style={styles.errorMessage}>
          {this.state.errorMessage && (
            <Text style={styles.error}>{this.state.errorMessage}</Text>
          )}
        </View>

        <View style={styles.form}>
          <View>
            <Text style={styles.inputTitle}>Email Address</Text>
            <TextInput 
              style={styles.input} 
              autoCapitalize="none" 
              onChangeText={(email) => this.setState({email})} 
              value={this.state.email}>
            </TextInput>
          </View>

          <View style={{marginTop: 31}}>
            <Text style={styles.inputTitle}>Password</Text>
            <TextInput
              style={styles.input}
              secureTextEntry
              autoCapitalize="none"
              onChangeText={(password) => this.setState({password})}
              value={this.state.password}>
            </TextInput>
          </View>
        </View>

        <TouchableOpacity style={styles.button}
          onPress={this.handleLogin}>
          <Text style={{color: '#fff', fontWeight: '500'}}>Sign in</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{alignSelf: 'center', marginTop: 31}}onPress={() => RootNavigation.navigate('Register')}>
          <Text style={{color: '#414959', fontSize: 13}}>New to Doccs?</Text>
          <Text style={{fontWeight: '500', color: '#E9446A'}}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {

  },
  greeting: {
    marginTop: 33,
    fontSize: 17,
    fontWeight: '400',
    textAlign: 'center',
  },
  errorMessage: {
    height: 70,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 29,
  },
  error: {
    color: '#E9446A',
    fontSize: 13,
    fontWeight: '600',
    textAlign: 'center',
  },
  form: {
    marginBottom: 48,
    marginHorizontal: 30,
  },
  inputTitle: {
    color: '#8A8F9E',
    fontSize: 10,
    textTransform: 'uppercase',
  },
  input: {
    borderBottomColor: '#8A8F9E',
    borderBottomWidth: StyleSheet.hairlineWidth,
    height: 40,
    fontSize: 15,
    color: '#161F3D',
  },
  button: {
    marginHorizontal: 30,
    backgroundColor: '#E9446A',
    borderRadius: 4,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
