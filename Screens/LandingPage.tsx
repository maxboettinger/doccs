import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  TouchableOpacity
} from 'react-native';
import {connect} from 'react-redux';
import Footer from './Footer';


declare var global: {HermesInternal: null | {}};

interface Props {
  displayName: string;
  email: string;
  isLoggedIn: boolean;
  operating_system: any;
}
class LandingPage extends React.Component<Props> {
  componentDidMount() {

  }

  render() {
    console.log("- - - REDUX STATE - - - ")
    console.log(this.props.isLoggedIn)
    console.log(this.props.email)
    console.log(this.props.displayName)
    console.log(this.props.operating_system)
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.wrapper}>
          <ScrollView style={styles.content}>
            <View style={styles.content_item}>
              <Text>You added a new measure!</Text>
              <Text>Weight: 211 pound</Text>
            </View>
            <View style={styles.content_item}>
              <Text>You added a new measure!</Text>
              <Text>Height: 1.65m</Text>
            </View>
            <View style={styles.content_item}>
              <Text>You connected this App to Apple Health!</Text>
              <Text>Let's get healthy together</Text>
            </View>
            <View style={styles.content_item}>
              <Text>You logged in for the first time!</Text>
              <Text>Welcome to doccs</Text>
            </View>
          </ScrollView>
          {this.props.isLoggedIn ? <Footer></Footer> : null}
        </SafeAreaView>
      </>
    );
  }
};
function mapStateToProps(state:any) {
  return {
    email: state.email, 
    displayName: state.displayName,
    isLoggedIn: state.isLoggedIn,
    operating_system: state.operating_system
  }
}
export default connect(mapStateToProps)(LandingPage)


const styles = StyleSheet.create({
    header: {
        flex: 1,
        backgroundColor: '#E0E0E0',
    
      },
      wrapper: {
        flex: 0,
        flexDirection: 'column',
        color: 'black',
        backgroundColor: 'white',
        height: '100%'
      },
      content: {
        flex: 20,
        flexBasis: 590,
        flexDirection: 'column'
      },
      content_item: {
        flex: 1,
        padding: 30,
        backgroundColor: '#ecf0f1',
        borderRadius: 30,
        margin: 20
      },
      footer: {
        flex: 1
      }
});
