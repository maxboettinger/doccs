import 'react-native-gesture-handler';
import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { navigationRef } from './services/RootNavigation';
import * as firebase from 'firebase'
import { createStore } from 'redux'
import { Provider } from 'react-redux'

// Custom Components
import LoadingScreen from './Screens/LoadingScreen'
import LoginScreen from './Screens/LoginScreen'
import RegisterScreen from './Screens/RegisterScreen'
import LandingPage from './Screens/LandingPage'
import Profile from './Screens/profile'

// firebase
 var firebaseConfig = {
    apiKey: "AIzaSyDYVF8D7NjDu6aET3R5-rDzxfnFqNrfSKo",
    authDomain: "doccs1.firebaseapp.com",
    databaseURL: "https://doccs1.firebaseio.com",
    projectId: "doccs1",
    storageBucket: "doccs1.appspot.com",
    messagingSenderId: "1054718333776",
    appId: "1:1054718333776:web:6c55f55d38d33880801255"
  };

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

// redux 
const initialState = {
  email: "redux not working", 
  displayName: "redux not working",
  isLoggedIn: false,
  operating_system: 'strange'
}
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_USER_INFO':
      return { displayName: action.value.displayName, email: action.value.email, isLoggedIn: action.value.isLoggedIn, operating_system: action.value.operating_system }
  }
  return state
}
let store = createStore(reducer)

// navigation
const Stack = createStackNavigator()
declare var global: {HermesInternal: null | {}};




export default class App extends React.Component {
  render() {
    return (
      <>
      <Provider store={store}>
        <NavigationContainer ref={navigationRef}>
          <View style={styles.navigator}>
            <Stack.Navigator>
            <Stack.Screen
                name="Loading"
                component={LoadingScreen}
                options={{title: 'Feed', headerLeft: () => {
                  return null;
                }}}
              />
            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{title: 'Feed', headerLeft: () => {
                  return null;
                }}}
              />
              <Stack.Screen
              name="Register"
              component={RegisterScreen}
              options={{title: 'Register', headerLeft: () => {
                return null;
              }}}
              />
              <Stack.Screen
                name="Feed"
                component={LandingPage}
                options={{title: 'Feed', headerLeft: () => {
                  return null;
                }}}
              />
              <Stack.Screen 
                name="Profile" 
                component={Profile} 
                options={{title: 'Profile', headerLeft: () => {
                  return null;
                }}}
              />
            </Stack.Navigator>
            </View>
          </NavigationContainer>
        </Provider>
      </>
    );
  }
};

const styles = StyleSheet.create({
  navigator: {
    flex: 5
  }
});