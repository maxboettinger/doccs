import * as React from 'react';

import AppleHealthKit, { HealthUnit } from 'rn-apple-healthkit';
let options = {
  permissions: {
      read: ["Height", "Weight", "DateOfBirth", "HeartRate", "BiologicalSex", "ActiveEnergyBurned", "BodyTemperature", "RespiratoryRate", "SleepAnalysis", "StepCount"],
      write: []
  }
};

AppleHealthKit.initHealthKit(options, (err, results) => {
  if (err) {
      console.log("error initializing Healthkit: ", err);
      return;
  } else {
    console.log("HealthKit is ready!")
  }
});

// Getter and Setter Methods for all Data Points

export async function get_height() {
  await AppleHealthKit.getLatestHeight({unit: 'meter'}, (err: string, results: Object) => {
    if (err) {
        console.log("error getting latest height: ", err);
        return;
    }
    return results
  });
}

export async function get_DOB() {
  let result;
  AppleHealthKit.getDateOfBirth(null, (err, results) => {
    return results
  });
  //return result
}